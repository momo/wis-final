import feedparser
from app import db
from app.models import Feed,Item
from datetime import datetime
from time import mktime

__author__ = 'Momo'


def addFeed(url,title):
    feed = feedparser.parse(url,title)
    Feed_db=Feed(url=url,title=title)
    if feed.bozo==1:
        return False

    for objet in feed.entries:

        timestamp = datetime.utcnow()

            #verification si une date est disponible
        if 'published_parsed' in objet:
            print "published"
            timestamp = datetime.fromtimestamp(mktime(objet.published_parsed))

        elif 'updated_parsed' in objet:
            print "updated"
            timestamp = datetime.fromtimestamp(mktime(objet.updated_parsed))

        print timestamp

        data = Item(date=timestamp,link=objet.link,title=objet.title,summary=objet.summary,feed=Feed_db)

        #Verification que le item d'un feed est unique
        if not Item.query.filter(Item.link == data.link).first():
            db.session.add(data)
            db.session.commit()




def updateFeeds():
    for Feed_db in Feed.query.all():
        feed = feedparser.parse(Feed_db.url,Feed_db.title)
        if feed.bozo == 1:
            return False

        for objet in feed.entries:

            timestamp = datetime.utcnow()

            #verification si une date est disponible
            if 'published_parsed' in objet:
                print "published"
                timestamp = datetime.fromtimestamp(mktime(objet.published_parsed))

            elif 'updated_parsed' in objet:
                print "updated"
                timestamp = datetime.fromtimestamp(mktime(objet.updated_parsed))

            print timestamp


            #Verification que le item d'un feed est unique
            if not Item.query.filter_by(link=objet.link).first():
                data = Item(date=timestamp,link=objet.link,title=objet.title,summary=objet.summary,feed=Feed_db)
                db.session.add(data)
                db.session.commit()





if  __name__ == '__main__':
    updateFeeds()

