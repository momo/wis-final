from hashlib import md5
from app import db
from app import app
import flask.ext.whooshalchemy as whooshalchemy
import re

ROLE_USER = 0
ROLE_ADMIN = 1

followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)

tags = db.Table('tags',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
    db.Column('feed_id', db.Integer, db.ForeignKey('feed.id')),
    db.Index('idx_col12','tag_id','feed_id', unique=True)
)

category = db.Table('category',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
    db.Column('item_id', db.Integer, db.ForeignKey('item.id')),
    db.Index('idx_col12', 'tag_id', 'item_id',unique=True)
)


user_category = db.Table('user_category',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Index('idx_col12', 'tag_id', 'user_id',unique=True)
)


subscribed = db.Table('subscribed',
    db.Column('user_id',db.Integer, db.ForeignKey('user.id')),
    db.Column('feed_id',db.Integer, db.ForeignKey('feed.id'))
)

shared = db.Table('shared',
    db.Column('user_id',db.Integer, db.ForeignKey('user.id')),
    db.Column('item_id',db.Integer, db.ForeignKey('item.id'))
)
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    feeds = db.relationship('Feed', secondary=subscribed, backref='subscriber', lazy='dynamic')
    items = db.relationship('Item', secondary=shared, backref='share', lazy='dynamic')
    user_tags = db.relationship('Tag', secondary=user_category, backref='personal_tags', lazy='dynamic')
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime)
    oauth_token = db.Column(db.String(200))
    oauth_secret = db.Column(db.String(200))
    followed = db.relationship('User',
        secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref = db.backref('followers', lazy = 'dynamic'),
        lazy = 'dynamic')

    @staticmethod
    def make_valid_nickname(nickname):
        return re.sub('[^a-zA-Z0-9_\.]', '', nickname)

    @staticmethod
    def make_unique_nickname(nickname):
        if User.query.filter_by(nickname = nickname).first() == None:
            return nickname
        version = 2
        while True:
            new_nickname = nickname + str(version)
            if User.query.filter_by(nickname = new_nickname).first() == None:
                break
            version += 1
        return new_nickname

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def avatar(self, size):
        return 'http://www.gravatar.com/avatar/' + md5(self.email).hexdigest() + '?d=mm&s=' + str(size)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            return self

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            return self

    def is_following(self, user):
        return self.followed.filter(followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        return Post.query.join(followers, (followers.c.followed_id == Post.user_id)).filter(followers.c.follower_id == self.id).order_by(Post.timestamp.desc())

    def subscribe(self, feed):
        if not self.is_subscribed(feed):
            self.feeds.append(feed)
            return self

    def unsubscribe(self, feed):
        if self.is_subscribed(feed):
            self.feeds.remove(feed)
            return self

    def myitems(self):
        items = Item.query.join(Feed, (Feed.id == Item.feed_id)).join(subscribed, (Feed.id == subscribed.c.feed_id))
        return items.filter_by(user_id=self.id)

    def is_subscribed(self, feed):
        return self.feeds.filter(subscribed.c.feed_id == feed.id).count() > 0

    def share_item(self, item):
        if not self.is_shared(item):
            self.items.append(item)
            return self

    def is_shared(self, item):
        return self.items.filter(shared.c.item_id == item.id).count() > 0

    def shared_items(self):
        return Item.query.join(shared, (shared.c.item_id == Item.id)).filter(shared.c.user_id == self.id).order_by(Item.date.desc())

    def remove_share(self, item):
        if self.is_shared(item):
            self.items.remove(item)
            return self

    def has_share(self):
        return self.items.count() > 0

    def has_subscription(self):
        return self.feeds.count() > 0

    def addtag(self, tag):
        if not self.has_tag(tag):
            self.user_tags.append(tag)
            return self

    def removetag(self, tag):
        if self.has_tag(tag):
            self.user_tags.remove(tag)
            return self

    def has_tag(self, tag):
        return self.user_tags.filter(user_category.c.tag_id == tag.id).count() > 0

    def get_tags(self):
        return self.user_tags.all()

    def __repr__(self):
        return '<Item %r>' % (self.summary)

    def __repr__(self):
        return '<User %r>' % (self.nickname)

class Post(db.Model):
    __searchable__ = ['body']

    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    language = db.Column(db.String(5))

    def __repr__(self):
        return '<Post %r>' % (self.body)





class Feed(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(100) , unique=True)
    title = db.Column(db.String(100) , unique=True)
    tags = db.relationship('Tag',secondary=tags,backref='feeds',lazy='dynamic')
    items = db.relationship('Item', backref = 'feed', lazy = 'dynamic')

    def __repr__(self):
        return '<Feed %r>' %(self.url)

    def addtag(self, tag):
        if not self.has_tag(tag):
            self.tags.append(tag)
            return self

    def removetag(self, tag):
        if self.has_tag(tag):
            self.tags.remove(tag)
            return self

    def has_tag(self, tag):
        return self.tags.filter(tags.c.tag_id == tag.id).count() > 0

    def get_tags(self):
        return self.tags.all()


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date= db.Column(db.DateTime)
    link = db.Column(db.String(100) ,unique=True)
    title = db.Column(db.String(100))
    summary = db.Column(db.String(250))
    category = db.relationship('Tag', secondary=category, backref='items', lazy='dynamic')
    feed_id = db.Column(db.Integer, db.ForeignKey('feed.id'))

    def addtag(self, tag):
        if not self.has_tag(tag):
            self.category.append(tag)
            return self

    def removetag(self, tag):
        if self.has_tag(tag):
            self.category.remove(tag)
            return self

    def has_tag(self, tag):
        return self.category.filter(category.c.tag_id == tag.id).count() > 0

    def get_tags(self):
        return self.category.all()

    def get_personal_tags(self, user):
        return Tag.query.join(ItemTagUser, (ItemTagUser.tag_id == Tag.id)).filter_by(item_id=self.id, user_id=user.id).all()

    def __repr__(self):
        return '<Item %r>' % (self.summary)

class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __repr__(self):
        return '<Tag %r>' % (self.name)


class ItemTagUser(db.Model):
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    tag_id = db.Column(db.Integer, db.ForeignKey('tag.id'), primary_key=True)

    items = db.relationship("Item")
    users = db.relationship("User")
    tags = db.relationship("Tag")

    @staticmethod
    def removeTagUserItem(user, tag, item):
        if ItemTagUser.haveTagUserItem(user, tag, item):
            ItemTagUser.query.filter_by(item_id=item.id, user_id=user.id, tag_id=tag.id).delete()

    @staticmethod
    def haveTagUserItem(user, tag, item):
        return ItemTagUser.query.filter_by(item_id=item.id, user_id=user.id, tag_id=tag.id).count() > 0

    @staticmethod
    def addTagUserItem(user, tag, item):
        if not ItemTagUser.haveTagUserItem(user, tag, item):
            return ItemTagUser(item_id=item.id, user_id=user.id, tag_id=tag.id)



whooshalchemy.whoosh_index(app, Post)
