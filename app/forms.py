from flask.ext.wtf import Form, TextField, BooleanField, TextAreaField, SelectMultipleField, SubmitField,HiddenField
from flask.ext.wtf import Required, Length,url
from flask.ext.wtf.html5 import  URLField
from flask.ext.babel import gettext
from app.models import User, Tag, Feed
from feedparser import parse

class LoginForm(Form):
    openid = TextField('openid', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)


class EditForm(Form):
    nickname = TextField('nickname', validators = [Required()])
    about_me = TextAreaField('about_me', validators = [Length(min = 0, max = 140)])
    
    def __init__(self, original_nickname, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.original_nickname = original_nickname
        
    def validate(self):
        if not Form.validate(self):
            return False
        if self.nickname.data == self.original_nickname:
            return True
        if self.nickname.data != User.make_valid_nickname(self.nickname.data):
            self.nickname.errors.append(gettext('This nickname has invalid characters. Please use letters, numbers, dots and underscores only.'))
            return False
        user = User.query.filter_by(nickname = self.nickname.data).first()
        if user != None:
            self.nickname.errors.append(gettext('This nickname is already in use. Please choose another one.'))
            return False
        return True
        

class PostForm(Form):
    post = TextField('post', validators = [Required()])
    

class SearchForm(Form):
    search = TextField('search', validators = [Required()])


class FeedForm(Form):
    feedUrl = URLField('url',validators=[url(), Required()])
    name = TextField('name',validators=[Required()])
    tags = SelectMultipleField('tags', coerce=int)
    single_tag= TextField('otag')
    def __init__(self, feed=None,*args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.tags.choices = [(g.id, g.name) for g in Tag.query.all()]
        if feed:
            self.tags.default = [(g.id) for g in feed.tags.all()]
            self.feedUrl.data=feed.url
            self.name.data=feed.title

    def validate(self):
        if not Form.validate(self):
            return False
        parsing = parse(self.feedUrl.data)
        if parsing.bozo == 1:
            self.feedUrl.errors.append(gettext('The provided Url does not contain a valid RSS or Atom Feed.'))
            return False

        feed = Feed.query.filter_by(title = self.name.data).first()
        feed2 = Feed.query.filter_by(url = self.feedUrl.data).first()
        if not feed:
            self.name.errors.append(gettext('The entered name is not unique'))

        if not feed2:
            self.feedUrl.errors.append(gettext('There is an existing feed with the provided url'))

        if not self.single_tag.data  and not self.tags.data:
            self.tags.errors.append(gettext('You must at least select a tag'))
            self.single_tag.errors.append(gettext('You must at least select a tag'))
            return False

        if self.name.data=="":
            self.name.errors.append(gettext('Name is required'))
            return False
        return True

class ItemForm(Form):

    tags = SelectMultipleField('tags', coerce=int)
    single_tag= TextField('otag')
    def __init__(self, item=None,*args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.tags.choices = [(g.id, g.name) for g in Tag.query.all()]
        if item:
            self.tags.default = [(g.id) for g in item.category.all()]


    def validate(self):
        if not Form.validate(self):
            return False


        if not self.single_tag.data  and not self.tags.data:
            self.tags.errors.append(gettext('You must at least select a tag'))
            self.single_tag.errors.append(gettext('You must at least select a tag'))
            return False

        return True