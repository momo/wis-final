from flask import render_template, flash, redirect, session, url_for, request, g, jsonify
from flask.ext.login import login_user, logout_user, current_user, login_required
from flask.ext.babel import gettext
from app import app, db, lm, oid, babel,twitter
from forms import LoginForm, EditForm, PostForm, SearchForm, FeedForm,ItemForm
from models import User,Feed,Item, ROLE_USER, ROLE_ADMIN, Post, Tag, tags,ItemTagUser
from datetime import datetime
from emails import follower_notification
from guess_language import guessLanguage
from translate import microsoft_translate
from config import POSTS_PER_PAGE, MAX_SEARCH_RESULTS, LANGUAGES
from flask.ext.restless import APIManager,ProcessingException

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())
    
@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = get_locale()

@app.errorhandler(404)
def internal_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500

@app.route('/', methods = ['GET', 'POST'])
@app.route('/index', methods = ['GET', 'POST'])
@app.route('/index/<int:page>', methods = ['GET', 'POST'])
@login_required
def index(page = 1):
    form = PostForm()
    if form.validate_on_submit():
        language = guessLanguage(form.post.data)
        if language == 'UNKNOWN' or len(language) > 5:
            language = ''
        post = Post(body = form.post.data,
            timestamp = datetime.utcnow(),
            author = g.user,
            language = language)
        db.session.add(post)
        db.session.commit()
        flash(gettext('Your post is now live!'))
        return redirect(url_for('index'))
    posts = g.user.followed_posts().paginate(page, POSTS_PER_PAGE, False)
    return render_template('index.html',
        title = 'Home',
        form = form,
        posts = posts)

#User views

@app.route('/login', methods = ['GET', 'POST'])
@oid.loginhandler
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        session['remember_me'] = form.remember_me.data
        return oid.try_login(form.openid.data, ask_for = ['nickname', 'email'])
    return render_template('login.html', 
        title = 'Sign In',
        form = form,
        providers = app.config['OPENID_PROVIDERS'])

@oid.after_login
def after_login(resp):
    if resp.email is None or resp.email == "":
        flash(gettext('Invalid login. Please try again.'))
        return redirect(url_for('login'))
    user = User.query.filter_by(email = resp.email).first()
    if user is None:
        nickname = resp.nickname
        if nickname is None or nickname == "":
            nickname = resp.email.split('@')[0]
        nickname = User.make_valid_nickname(nickname)
        nickname = User.make_unique_nickname(nickname)
        user = User(nickname = nickname, email = resp.email, role = ROLE_USER)
        db.session.add(user)
        db.session.commit()
        # make the user follow him/herself
        db.session.add(user.follow(user))
        db.session.commit()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember = remember_me)
    return redirect(request.args.get('next') or url_for('index'))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
    
@app.route('/user/<nickname>')
@app.route('/user/<nickname>/<int:page>')
@login_required
def user(nickname, page = 1):
    user = User.query.filter_by(nickname = nickname).first()
    if user == None:
        flash(gettext('User %(nickname)s not found.', nickname = nickname))
        return redirect(url_for('index'))
    posts = user.posts.paginate(page, POSTS_PER_PAGE, False)
    return render_template('user.html',
        user = user,
        posts = posts)

@app.route('/users/')
@login_required
def users():
    users = User.query.all()
    if users == None:
        #flash(gettext('User %(nickname)s not found.', nickname = nickname))
        return redirect(url_for('index'))
    return render_template('users.html',
        users = users)


@app.route('/edit', methods = ['GET', 'POST'])
@login_required
def edit():
    form = EditForm(g.user.nickname)
    if form.validate_on_submit():
        g.user.nickname = form.nickname.data
        g.user.about_me = form.about_me.data
        db.session.add(g.user)
        db.session.commit()
        flash(gettext('Your changes have been saved.'))
        return redirect(url_for('edit'))
    elif request.method != "POST":
        form.nickname.data = g.user.nickname
        form.about_me.data = g.user.about_me
    return render_template('edit.html',
        form = form)



@app.route('/follow/<nickname>')
@login_required
def follow(nickname):
    user = User.query.filter_by(nickname = nickname).first()
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))
    if user == g.user:
        flash(gettext('You can\'t follow yourself!'))
        return redirect(url_for('user', nickname = nickname))
    u = g.user.follow(user)
    if u is None:
        flash(gettext('Cannot follow %(nickname)s.', nickname = nickname))
        return redirect(url_for('user', nickname = nickname))
    db.session.add(u)
    db.session.commit()
    flash(gettext('You are now following %(nickname)s!', nickname = nickname))
    follower_notification(user, g.user)
    return redirect(url_for('user', nickname = nickname))


@app.route('/unfollow/<nickname>')
@login_required
def unfollow(nickname):
    user = User.query.filter_by(nickname = nickname).first()
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))
    if user == g.user:
        flash(gettext('You can\'t unfollow yourself!'))
        return redirect(url_for('user', nickname = nickname))
    u = g.user.unfollow(user)
    if u is None:
        flash(gettext('Cannot unfollow %(nickname)s.', nickname = nickname))
        return redirect(url_for('user', nickname = nickname))
    db.session.add(u)
    db.session.commit()
    flash(gettext('You have stopped following %(nickname)s.', nickname = nickname))
    return redirect(url_for('user', nickname = nickname))

#Item Sharing Views

@app.route('/share/', methods = ['POST'])
@login_required
def share():
    item = request.form['item']
    item_db = Item.query.filter_by(id = item).first()
    if item_db == None:
        flash('Item ' + item + ' not found.')
        return redirect(url_for('index'))
    if g.user.is_shared(item_db):
        flash(gettext('You are already shared that item!'))
        return redirect(url_for('show_feed', feed = item_db.feed.title))
    u = g.user.share_item(item_db)
    if u is None:
        flash(gettext('You cannot share that item'))
        return redirect(url_for('show_feed', feed = item_db.feed.title))


    db.session.commit()
    flash(gettext('You are sharing %(item)s!', item = item_db.title))
    return redirect(url_for('show_feed', feed= item_db.feed.title))


@app.route('/unshare/', methods = ['POST'])
@login_required
def unshare():
    item = request.form['item']
    item_db = Item.query.filter_by(id = item).first()
    if item_db == None:
        flash('Item ' + item_db + ' not found.')
        return redirect(url_for('user_share'))

    if not g.user.is_shared(item_db):
        flash(gettext('You are not sharing that item!'))
        return redirect(url_for('user_share'))

    u = g.user.remove_share(item_db)
    if u is None:
        flash(gettext('You cannot unshare that item'))
        return redirect(url_for('user_share'))


    db.session.commit()
    flash(gettext('You removed your share for %(item)s!', item = item_db.title))
    return redirect(url_for('user_share'))


#Feed Views

@app.route('/subscribe/', methods = ['POST'])
@login_required
def subscribe():
    feed = request.form['feed']
    feed_db = Feed.query.filter_by(id = feed).first()
    if feed_db == None:
        flash('Feed ' + feed + ' not found.')
        return redirect(url_for('index'))
    if g.user.is_subscribed(feed_db):
        flash(gettext('You are already subscribed to that feed!'))
        return redirect(url_for('show_feed', feed = feed_db.title))
    u = g.user.subscribe(feed_db)
    if u is None:
        flash(gettext('You cannot subscribe to this feed'))
        return redirect(url_for('show_feed', feed = feed_db.title))


    db.session.commit()
    flash(gettext('You are now subscribed to %(feed)s!', feed = feed_db.title))
    return redirect(url_for('show_feed', feed=feed_db.title))


@app.route('/unsubscribe/', methods = ['POST'])
@login_required
def unsubscribe():
    feed = request.form['feed']
    feed_db = Feed.query.filter_by(id = feed).first()
    if feed_db == None:
        flash('Feed ' + feed + ' not found.')
        return redirect(url_for('show_feeds'))
    if not g.user.is_subscribed(feed_db):
        flash(gettext('You are already unsubscribed to that feed!'))
        return redirect(url_for('show_feeds'))
    u = g.user.unsubscribe(feed_db)
    if u is None:
        flash(gettext('You cannot unsubscribe from this feed', title = feed_db.title))
        return redirect(url_for('show_feeds'))


    db.session.commit()
    flash(gettext('You are now unsubscribed to %(feed)s!', feed = feed_db.title))
    return redirect(url_for('user_subscription'))

#Showing Feed, By Tags

@app.route('/feed/')
@app.route('/feed/tag/<tag>/')
@app.route('/feed/tag/<tag>/<int:page>')
@app.route('/feed/<int:page>/')
@login_required
def show_feeds(tag='',page=1):


    tag_db= Tag.query.filter_by(name=tag).first()
    feed_db = Feed.query.paginate(page, POSTS_PER_PAGE, False)

    if tag_db:
        feed_db = Feed.query.join(tags, (tags.c.feed_id == Feed.id)).filter_by(tag_id=tag_db.id).paginate(page, POSTS_PER_PAGE, False)

    return render_template('index_feed.html',
        feeds = feed_db)



# User subscribed feed

@app.route('/user/subscrption/')
@app.route('/user/subscrption/<int:page>')
@login_required
def user_subscription(page=1):

    feed_db = g.user.feeds.paginate(page, POSTS_PER_PAGE, False)

    if not feed_db.items:
        flash(gettext("You have no subscriptions, subscribe first to a feed"))
        return redirect(url_for('show_feeds'))

    return render_template('index_feed.html',
        feeds = feed_db)

@app.route('/user/subscrption/feed/')
@app.route('/user/subscrption/feed/<int:page>')
@login_required
def user_subscription_feed(page=1):

    items_db = g.user.myitems().order_by("date desc").paginate(page, POSTS_PER_PAGE, False)
    feed = {'title':g.user.nickname+" Feed"}
    return render_template('user_feed.html',
        feed = feed,
        items = items_db)


# User shared items


@app.route('/user/shared/')
@app.route('/user/shared/<int:page>')
@app.route('/<nickname>/shared/')
@app.route('/<nickname>/shared/<int:page>')
@login_required
def user_share(nickname='user', page=1):

    if nickname == 'user':
        nickname = g.user.nickname

    user_db = User.query.filter_by(nickname=nickname).first()
    if not user_db:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))

    if g.user.id == user_db.id or g.user.is_following(user_db):
        items_db = user_db.shared_items().paginate(page, POSTS_PER_PAGE, False)

        if not items_db.items:
            flash(gettext("%(username)s is currently not sharing items for the moment",username=user_db.nickname))
            return redirect(url_for('user_subscription'))

        return render_template('user_share.html',
        items = items_db,
        user = user_db)


    if not g.user.is_following(user_db):
        flash('You need to follow ' + nickname + ' first.')
        return redirect(url_for('user', nickname=user_db.nickname))

    items_db = g.user.shared_items().paginate(page, POSTS_PER_PAGE, False)

    return render_template('user_share.html',
        items = items_db,
        user = user_db)

@app.route('/tag/<tag>/')
@app.route('/tag/<tag>/<int:page>')
@login_required
def user_tag_personal(tag, page=1):

    tag_db=Tag.query.filter_by(name=tag).join(ItemTagUser, (ItemTagUser.tag_id == Tag.id)).first()


    if not tag_db:
        flash('You have not tagged any item with the tag' + tag)
        #items_db = user_db.shared_items().paginate(page, POSTS_PER_PAGE, False)
        return redirect(url_for('index'))

    items_db = Item.query.join(ItemTagUser, (ItemTagUser.item_id == Item.id)).filter_by(tag_id=tag_db.id,user_id=g.user.id)
    items_db = items_db.paginate(page, POSTS_PER_PAGE, False)

    return render_template('user_tag_personal.html',
        items = items_db,
        tag = tag_db)


@app.route('/feed/<feed>/')
@app.route('/feed/<feed>/<int:page>')
@login_required
def show_feed(feed, page=1):
    feed_db = Feed.query.filter_by(title = feed).first()
    if feed_db == None:
        flash('Feed ' + feed + ' not found.')
        return redirect(url_for('show_feeds'))

    if not g.user.is_subscribed(feed_db):
        flash('Subscribe to the feed' + feed + ' before viewing.')
        return redirect(url_for('show_feeds'))


    items = feed_db.items.order_by("date desc").paginate(page, POSTS_PER_PAGE, False)

    return render_template('feed.html',
        feed = feed_db,
        items = items)




@app.route('/add/feed/', methods = ['GET', 'POST'])
@app.route('/add/feed/<feed>', methods = ['GET', 'POST'])
@app.route('/feed/<feed>/add', methods = ['POST'])
@login_required
def add_feed(feed=""):
    feed_db = Feed.query.filter_by(title = feed).first()
    if feed_db == None:

        form = FeedForm()
        if feed:
            form.name.data = feed

    elif feed_db:
        return redirect(url_for('edit_feed', feed=feed))

    if form.validate_on_submit():
        feed_db=Feed(title=form.name.data,url=form.feedUrl.data)


        #mis a jour des nouveaux tag
        for tag in form.tags.data:
            feed_db.tags.append(Tag.query.filter_by(id=tag).first())

        if form.single_tag.data!='':
            tag_db = Tag(name = form.single_tag.data)

            #association d'un tag existant
            tag_db_old=Tag.query.filter_by(name = form.single_tag.data).first()

            if tag_db_old:
                feed_db.tags.append(tag_db_old)

            #ajout d'un nouveau tag
            else:
                feed_db.tags.append(tag_db)

        flash(gettext('Your %(title)s have been added .', title = feed_db.title))
        db.session.add(feed_db)
        db.session.commit()

        from feed_update import updateFeeds

        updateFeeds()

        return redirect(url_for('show_feeds'))
        #return redirect(url_for('edit_feed', feed=feed_db.title))


    return render_template('addFeed.html',
        form = form)

@app.route('/feed/<feed>/edit', methods = ['GET', 'POST'])
@login_required
def edit_feed(feed):
    if g.user.role == ROLE_ADMIN:
        feed_db = Feed.query.filter_by(title = feed).first()

        form = FeedForm(feed_db)
        if feed_db == None:

            return redirect(url_for('index'))

        if form.validate_on_submit():

            old_tags = feed_db.tags

            #effacement des tag precedant

            new_tags = []
            #mis a jour des nouveaux tag
            for tag in form.tags.data:
                    #association d'un tag existant
                    tag_db=Tag.query.filter_by(id=tag).first()

                    if tag_db:
                        new_tags.append(tag_db)



            if form.single_tag.data != '':
                tag_db = Tag(name=form.single_tag.data)
                flash(form.single_tag.data.split(","))
                #association d'un tag existant
                tag_db_old=Tag.query.filter_by(name=form.single_tag.data).first()

                if tag_db_old:
                    new_tags.append(tag_db_old)
                #ajout d'un nouveau tag

                else:
                    new_tags.append(tag_db)
                    #db.session.add(tag_db)
            ## tag a effacer
            tag_delete = list(set(old_tags).difference(set(new_tags)))

            ## tag a ajouter
            tag_add = list(set(new_tags).difference(set(old_tags)))

            for tag in tag_delete:

                feed_db.removetag(tag)

            var =[]
            for tag in tag_add:
                var.append(str(tag.name))
                feed_db.addtag(tag)

            #db.session.commit()


            flash(gettext('These tags have been added %(Tags)s', Tags=var))
            flash(gettext('Your changes have been saved.'))

        return render_template('addFeed.html', form=form)

    flash(gettext('Only admin can access this page.'))
    return redirect(url_for('index'))

@app.route('/item/<item>/edit', methods = ['GET', 'POST'])
@login_required
def edit_item(item):
    if g.user:
        item_db = Item.query.filter_by(id=item).first()

        form = ItemForm(item_db)
        if item_db == None:

            return redirect(url_for('index'))

        if form.validate_on_submit():

            old_tags = item_db.category

            #effacement des tag precedant

            new_tags = []
            #mis a jour des nouveaux tag
            for tag in form.tags.data:
                    #association d'un tag existant
                    tag_db=Tag.query.filter_by(id=tag).first()

                    if tag_db:
                        new_tags.append(tag_db)



            if form.single_tag.data != '':


                #ajout de tag separe par des virgules
                for tag in form.single_tag.data.split(","):
                #association d'un tag existant
                    tag_db = Tag(name=tag)
                    tag_db_old=Tag.query.filter_by(name=tag).first()

                    if tag_db_old:
                        new_tags.append(tag_db_old)

                    #ajout d'un nouveau tag

                    else:
                        new_tags.append(tag_db)
                        db.session.add(tag_db)

            ## tag a effacer
            tag_delete = list(set(old_tags).difference(set(new_tags)))

            ## tag a ajouter
            tag_add = list(set(new_tags).difference(set(old_tags)))

            for tag in tag_delete:
                ItemTagUser.removeTagUserItem(g.user, tag, item_db)
                item_db.removetag(tag)

            var =[]
            for tag in tag_add:
                var.append(str(tag.name))
                item_db.addtag(tag)
                item_tag_user_db=ItemTagUser.addTagUserItem(g.user, tag, item_db)
                g.user.addtag(tag)
                db.session.add(item_tag_user_db)

            db.session.commit()


            flash(gettext('These tags have been added %(Tags)s', Tags=var))
            flash(gettext('Your changes have been saved.'))

        return render_template('edititem.html', form=form, item=item_db)

    flash(gettext('Only admin can access this page.'))
    return redirect(url_for('index'))


@app.route('/search', methods = ['POST'])
@login_required
def search():
    if not g.search_form.validate_on_submit():
        return redirect(url_for('index'))
    return redirect(url_for('search_results', query = g.search_form.search.data))

@app.route('/search_results/<query>')
@login_required
def search_results(query):
    results = Post.query.whoosh_search(query, MAX_SEARCH_RESULTS).all()
    return render_template('search_results.html',
        query = query,
        results = results)

@app.route('/translate', methods = ['POST'])
@login_required
def translate():
    return jsonify({
        'text': microsoft_translate(
            request.form['text'],
            request.form['sourceLang'],
            request.form['destLang']) })


# Other webservices

@app.route('/oauth-authorized')
@twitter.authorized_handler
def oauth_authorized(resp):
    next_url = request.args.get('next') or url_for('index')
    if resp is None:
        flash(u'You denied the request to sign in.')
        return redirect(next_url)

    user = g.user

    user.oauth_token = resp['oauth_token']
    user.oauth_secret = resp['oauth_token_secret']
    db.session.commit()

    session['user_id'] = user.id
    flash(gettext('We added your Twitter permission'))
    return redirect(next_url)

@twitter.tokengetter
def get_twitter_token():
    user = g.user
    if user is not None:
        return user.oauth_token, user.oauth_secret

@app.route('/get_twitter')
def get_twitter():
    return twitter.authorize(callback=url_for('oauth_authorized',
        next=request.args.get('next') or request.referrer or None))

@app.route('/tweet', methods=['POST'])
def tweet():
    '''
Calls the remote twitter API to create a new status update.
'''
    if g.user is None:
        return redirect(url_for('index'))

    item = request.form['item']
    if not item:
        return redirect(url_for('index'))

    item_db=Item.query.filter_by(id=item).first()
    tags_db=item_db.get_personal_tags(g.user)

    resp = twitter.post('statuses/update.json', data={
        'status': "Partage "+item_db.title+" "+"de "+item_db.link
    })
    if resp.status == 403:
        flash('Your tweet was too long.')
    elif resp.status == 401:
        flash('Authorization error with Twitter.')
    else:
        flash(gettext('Successfully tweeted your tweet %(title)s',title=item_db.title))
    return redirect(url_for('index'))




def can_get(**kwargs):
    if not g.user.is_authenticated():
        raise ProcessingException(message='Authorized personel only')


def can_delete(**kwargs):
    if g.user.role!=ROLE_ADMIN or not g.user.is_authenticated():
        raise ProcessingException(message='Delete only for admin!')





manager = APIManager(app, flask_sqlalchemy_db=db)

manager.create_api(Item,methods=['GET','POST','DELETE'],results_per_page=4, include_columns=['date', 'link','title','summary','feed_id'],preprocessors={
                       'GET_SINGLE': [can_get],
                       'GET_MANY': [can_get],
                       'DELETE': [can_delete]
                       })

manager.create_api(Feed,methods=['GET','POST','DELETE'],results_per_page=4,include_columns=['url', 'title','items'], preprocessors={
                       'GET_SINGLE': [can_get],
                       'GET_MANY': [can_get],
                       'DELETE': [can_delete]
                       })

manager.create_api(Tag,methods=['GET','POST','DELETE'],results_per_page=4,include_columns=['name'], preprocessors={
                       'GET_SINGLE': [can_get],
                       'GET_MANY': [can_get],
                       'DELETE': [can_delete]
                       })

