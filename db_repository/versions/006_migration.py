from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
feed = Table('feed', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('url', String(length=100)),
    Column('title', String(length=100)),
)

item = Table('item', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('date', DateTime),
    Column('link', String(length=100)),
    Column('summary', String(length=250)),
    Column('feed_id', Integer),
)

shared = Table('shared', post_meta,
    Column('user_id', Integer),
    Column('item_id', Integer),
)

subscribed = Table('subscribed', post_meta,
    Column('user_id', Integer),
    Column('feed_id', Integer),
)

tag = Table('tag', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=50)),
)

tags = Table('tags', post_meta,
    Column('tag_id', Integer),
    Column('feed_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['feed'].create()
    post_meta.tables['item'].create()
    post_meta.tables['shared'].create()
    post_meta.tables['subscribed'].create()
    post_meta.tables['tag'].create()
    post_meta.tables['tags'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['feed'].drop()
    post_meta.tables['item'].drop()
    post_meta.tables['shared'].drop()
    post_meta.tables['subscribed'].drop()
    post_meta.tables['tag'].drop()
    post_meta.tables['tags'].drop()
