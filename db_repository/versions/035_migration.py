from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
lab_rat = Table('lab_rat', pre_meta,
    Column('tag_id', Integer),
    Column('user_id', Integer),
    Column('item_id', Integer),
)

item_tag_user = Table('item_tag_user', post_meta,
    Column('item_id', Integer, primary_key=True, nullable=False),
    Column('user_id', Integer, primary_key=True, nullable=False),
    Column('tag_id', Integer, primary_key=True, nullable=False),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['lab_rat'].drop()
    post_meta.tables['item_tag_user'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['lab_rat'].create()
    post_meta.tables['item_tag_user'].drop()
