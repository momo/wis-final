from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
user_category = Table('user_category', post_meta,
    Column('tag_id', Integer),
    Column('user_id', Integer),
)

category = Table('category', pre_meta,
    Column('tag_id', Integer),
    Column('item_id', Integer),
    Column('user_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['user_category'].create()
    pre_meta.tables['category'].columns['user_id'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['user_category'].drop()
    pre_meta.tables['category'].columns['user_id'].create()
